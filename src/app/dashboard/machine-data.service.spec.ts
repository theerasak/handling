import { TestBed, inject } from '@angular/core/testing';

import { MachineDataService } from './machine-data.service';

describe('MachineDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MachineDataService]
    });
  });

  it('should be created', inject([MachineDataService], (service: MachineDataService) => {
    expect(service).toBeTruthy();
  }));
});
