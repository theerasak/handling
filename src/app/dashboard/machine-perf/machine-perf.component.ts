import { Component, OnInit, AfterContentInit, Input, OnDestroy  } from '@angular/core';
import * as d3 from 'd3';
import * as d3ln from 'd3-timeline';
import { MachineDataService } from '../machine-data.service';
import { MachineListIPModel } from '../machine-list.service';
import { MachinePerfModel, MachineWorkingTimeModel} from '../shared/machineperf.model';
import * as moment from 'moment';
import { HandlingDataService } from '../handling-data.service';
import { DatePipe } from '@angular/common';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Rx';
import { Subscription } from "rxjs/Subscription";
import { JobStatusDashboardModel } from '../shared/jobstatusdashboard.model';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { isEmpty } from 'rxjs/operator/isEmpty';
import { isNull } from 'util';
import { map } from 'rxjs/operators';
import { MachineCommService, MachineCoummunicationInterface } from '../machine-comm.service';

@Component({
  selector: 'app-machine-perf',
  templateUrl: './machine-perf.component.html',
  styleUrls: ['./machine-perf.component.css']
})
export class MachinePerfComponent implements OnInit, AfterContentInit, OnDestroy {

  @Input()
  vMachine: MachineListIPModel;
  
  @Input()
  vIdentify: string;

  graph_date: Date;
  data_min: Date;
  data_max: Date;

  shift_day_start_time = '0800';
  shift_day_finish_time = '2000';
  shift_night_start_time = '2000';

  hourly: Date[] = [];
  
  shift_day_start_break_time = '1000';
  shift_night_start_break_time = '2200';
  diff_start_break_minute: number[] = [0, 125, 300, 450];
  diff_break_minute: number[] = [15, 50, 15, 40];
  start_break_time: Date[] = [];
  end_break_time: Date[] = [];

  sub: Subscription;
  timer = Observable.timer(0, 1200000);
  
  machineMessage: MachineCoummunicationInterface;

  constructor(private machineDS: MachineDataService,  private _datepipe: DatePipe
			, private _machineComm: MachineCommService) { }

  ngOnInit() {
	let currentDate: string;
	let shift_start_time: string;
	let break_start_time: string;
	let data_break_min: Date;

	this._machineComm.change.subscribe(msg => {
		this.machineMessage = msg;
		
		currentDate = this.machineMessage.productionDate;
		shift_start_time = (this.machineMessage.shift == 'Day') ? this.shift_day_start_time : this.shift_night_start_time;
		break_start_time = (this.machineMessage.shift == 'Day') ? this.shift_day_start_break_time : this.shift_night_start_break_time;
		
		this.data_min = moment(currentDate + shift_start_time, 'YYYYMMDDHHmm').toDate();
		this.data_max = moment(moment(this.data_min).add(12, 'hours').toDate()).add(10, 'minutes').toDate();
		
		data_break_min = moment(currentDate + break_start_time, 'YYYYMMDDHHmm').toDate();
		
		this.hourly.length = 0;		/* Clear old data	*/
		this.start_break_time.length = 0;
		this.end_break_time.length = 0;
		
		let machineIdentify: string;

		machineIdentify = this.vIdentify;

		d3.select('#container-' + this.vMachine.i_ind_content.trim() + '-' + machineIdentify).selectAll("*").remove();

		let i = 0;
		for (i = 0; i <= 12; i++ ) {
		  this.hourly.push(moment(this.data_min).add( i, 'hours').toDate());
		}
		
		for (i = 0; i <= 3; i++ ) {
			if(moment(data_break_min).add( this.diff_start_break_minute[i], 'minutes').valueOf() < moment().valueOf()){
				this.start_break_time.push(moment(data_break_min).add( this.diff_start_break_minute[i], 'minutes').toDate());
				this.end_break_time.push(moment(this.start_break_time[i]).add( this.diff_break_minute[i], 'minutes').toDate());
			}
		}
		
		/*	Call service for first time initial.
			Next time will subscribe in function 'ShowContent'.
		*/
		this.ShowContent(this.vMachine, machineIdentify);
	});
  }

  ngAfterContentInit() {
  }
  
  ngOnDestroy(){
	this.sub.unsubscribe();
	this._machineComm.change.unsubscribe();
  }
  
  ShowContent(_machine: MachineListIPModel, _identify: string) {

    const graph_width = 800;
    const graph_height = 140;
    const margin_graphLeft = 250;
    const margin_graphRight = 50;
    const margin_graphTop = 30;
    const bar_width = 60;
    const bar_gap = 10;

    let domain_min = moment(this.data_min).valueOf();
    let domain_max = moment(this.data_max).valueOf();
    let data: MachineWorkingTimeModel;
    let firstOffset: number;
    let machine: string
	let machine_name: string;
	
	let datasource: MachinePerfModel = {mcd: '', mc_desc: '', working: []};
	datasource.mcd = _machine.i_ind_content.trim();
	datasource.working = [];

    machine = _machine.i_ind_content.trim() + "-" + _identify;
	machine_name = _machine.machine_name.trim();
	
	let original_name = 'svg-' + machine;
    let name = '#' + original_name;

    let svg = d3.select('#container-' + machine)
        .append('svg')
        .attr('id', original_name)
        .attr('width',  graph_width)
        .attr('height', graph_height)
        .attr('style', 'outline: thin solid #f5f5dc');
	
	/* Object color linearGradient	*/
	let arrMain: string[] = ['mainGradientB', 'mainGradientW', 'mainGradientM', 'mainGradientO'];
	let colorTop: string[] = ['#ffaf4b', '#58fc46', '#f25a58', '#87e0fd'];
	let colorBot: string[] = ['#ff920a', '#34e021', '#d84240', '#05abe0'];
	
	arrMain.map((a, i) => {
		var svgDefs = svg.append('defs');
		var gradient = svgDefs.append('linearGradient')
				.attr('id', a)
				.attr("x1", "0%")
				.attr("y1", "0%")
				.attr("x2", "0%")
				.attr("y2", "100%");
		gradient.append('stop')
				.attr('stop-color', colorTop[i])
				.attr('offset', '0%');
		gradient.append('stop')
					.attr('stop-color', colorBot[i])
				.attr('offset', '100%');
	});
	/* --Object color linearGradient	*/
		
	svg = d3.select(name)
		.append('text')
		.attr('x', 10)
		.attr('y', 50)
		.attr('font-family', 'Tahoma')
		.attr('font-size', '2em')
		.attr('color', 'black')
		.text(machine_name);
		
	svg = d3.select(name)
		.append('text')
		.attr('x', 10)
		.attr('y', 100)
		.attr('font-family', 'Tahoma')
		.attr('font-size', '1.5em')
		.attr('color', 'black')
		.text('(' + _machine.ip_address.trim() +')');
				
				
	let xscale = d3.scaleLinear()
        .domain([domain_min, domain_max])
        .range([margin_graphLeft, graph_width]);
	
	xscale.clamp(true);
	
    svg = d3.select(name)
          .append('rect')
          .attr( 'x', xscale(moment(this.data_min).valueOf()) - margin_graphRight)
          .attr( 'y', 20)
          .attr( 'height', 2)
          .attr( 'width', xscale(moment(this.data_max).valueOf()) - xscale(moment(this.data_min).valueOf()))
          .attr( 'fill', '#dcdcdc');

    svg = d3.select(name);
	
	this.hourly.map ( function (d, i) {
		svg.append('text')
			.attr( 'x', xscale(moment(d).valueOf()) - margin_graphRight)
			.attr( 'y', 15)
			.attr('font-family', 'Tahoma')
			.attr('font-size', '12px')
			.attr('fill', 'gray')
			.text(moment(d).format('HH:mm'));
    });
	
	svg = d3.select(name)
		.append('rect')
		.attr( 'x', xscale(moment(this.data_min).valueOf()) - margin_graphRight)
		.attr( 'y', margin_graphTop)
		.attr( 'height', bar_width )
		.attr( 'width', xscale(moment().valueOf()) - xscale(moment(this.data_min).valueOf()))
		.attr( 'fill', '#e6e6e6');
		
	svg = d3.select(name);
	
	this.sub = this.timer.subscribe(t => {
		this.machineDS.getJobResultDashboardIP(this.machineMessage.productionDate, _machine.i_ind_content.trim(), _machine.ip_address.trim())
			.subscribe(data => {
				svg.selectAll('rect').remove();
				svg = d3.select(name)
					.append('rect')
					.attr( 'x', xscale(moment(this.data_min).valueOf()) - margin_graphRight)
					.attr( 'y', margin_graphTop)
					.attr( 'height', bar_width )
					.attr( 'width', xscale(moment().valueOf()) - xscale(moment(this.data_min).valueOf()))
					.attr( 'fill', '#e6e6e6');
					
				svg = d3.select(name);
				
				data.forEach( element => {
					let workingTime: MachineWorkingTimeModel = {type: '', jobno: '',
					item: '', start: new Date(), finish: new Date()};

					workingTime.type = element.type;
					workingTime.jobno = element.jobno;
					workingTime.item = element.i_sim_item_cd;
					workingTime.start = (moment(element.prc_start).valueOf() <= moment(this.data_min).valueOf()) ? this.data_min : element.prc_start;
					workingTime.finish = (moment(element.prc_end).valueOf() >= moment(this.data_max).valueOf()) ? this.data_max : element.prc_end;
					datasource.working.push(workingTime);
					
					svg.append('rect')
						.attr( 'class', workingTime.jobno)
						.attr( 'x', xscale(moment(workingTime.start).valueOf()) - margin_graphRight)
						.attr( 'y', margin_graphTop)
						.attr( 'height', bar_width )
						.attr( 'width', xscale(moment(workingTime.finish).valueOf()) - xscale(moment(workingTime.start).valueOf()))
						.attr( 'fill', 'url(#mainGradient' + workingTime.type + ')')
						.attr( 'stroke', 'url(#mainGradient' + workingTime.type + ')');
				});
				
				svg = d3.select(name);
				
				this.start_break_time.forEach((d, i) => {
					svg.append('rect')
						.attr( 'x', xscale(moment(d).valueOf()) - margin_graphRight)
						.attr( 'y', margin_graphTop)
						.attr( 'height', bar_width )
						.attr( 'width', xscale(moment(this.end_break_time[i]).valueOf()) - xscale(moment(d).valueOf()))
						.attr( 'fill', 'url(#mainGradientB)')
						.attr( 'stroke', 'url(#mainGradientB)');
				});
				
				svg.order();
			});
		
	});
  }

}