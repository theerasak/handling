import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachinePerfComponent } from './machine-perf.component';

describe('MachinePerfComponent', () => {
  let component: MachinePerfComponent;
  let fixture: ComponentFixture<MachinePerfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachinePerfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachinePerfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
