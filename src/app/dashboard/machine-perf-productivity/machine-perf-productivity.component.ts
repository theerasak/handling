import { Component, OnInit, AfterContentInit, Input, OnDestroy  } from '@angular/core';
import * as d3 from 'd3';
import * as d3ln from 'd3-timeline';
import { MachineDataService } from '../machine-data.service';
import { MachineListIPModel } from '../machine-list.service';
import { MachinePerfModel, MachineWorkingTimeModel} from '../shared/machineperf.model';
import * as moment from 'moment';
import { HandlingDataService } from '../handling-data.service';
import { DatePipe, PercentPipe } from '@angular/common';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Rx';
import { Subscription } from "rxjs/Subscription";
import { JobStatusDashboardModel } from '../shared/jobstatusdashboard.model';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { isEmpty } from 'rxjs/operator/isEmpty';
import { isNull } from 'util';
import { map } from 'rxjs/operators';
import { MachineCommService, MachineCoummunicationInterface } from '../machine-comm.service';

@Component({
  selector: 'app-machine-perf-productivity',
  templateUrl: './machine-perf-productivity.component.html',
  styleUrls: ['./machine-perf-productivity.component.css']
})
export class MachinePerfProductivityComponent implements OnInit, AfterContentInit, OnDestroy {

	@Input()
	vMachine: MachineListIPModel;
	
	@Input()
	vIdentify: string;
	
	graph_date: Date;
	data_min: Date;
	data_max: Date;

	shift_day_start_time = '0800';
	shift_day_finish_time = '2000';
	shift_night_start_time = '2000';
	
	sub: Subscription;
	timer = Observable.timer(0, 300000);

	machineMessage: MachineCoummunicationInterface;
	
	currentDate: string;
	shift_start_time: string;
	currentShift: string;


	constructor(private machineDS: MachineDataService,  private _datepipe: DatePipe
			, private _machineComm: MachineCommService, private percentPipe: PercentPipe) { }

	ngOnInit() {
		this._machineComm.change.subscribe(msg => {
			this.sub = null;
			
			this.machineMessage = msg;
			this.currentDate = this.machineMessage.productionDate;
			this.currentShift = this.machineMessage.shift;
			this.shift_start_time = (this.machineMessage.shift == 'Day') ? this.shift_day_start_time : this.shift_night_start_time;
			
			this.data_min = moment(this.currentDate + this.shift_start_time, 'YYYYMMDDHHmm').toDate();
			// Need create condition for real time
			this.data_max = this.checkCurrentDateShift(this.currentDate, this.currentShift) ? moment().toDate() 
				: moment(moment(this.data_min).add(12, 'hours').toDate()).add(10, 'minutes').toDate();
			//console.log("First " + this.data_max);
			d3.select('#prod-container-' + this.vMachine.i_ind_content.trim() + '-' + this.vIdentify).selectAll("*").remove();
			
			/*	Call service for first time initial.
				Next time will subscribe in function 'ShowContent'.
			*/
			this.ShowContent(this.vMachine, this.vIdentify)
		});
	}

	ngAfterContentInit() {

	}
	
	private checkCurrentDateShift(paramDate: string, paramShift: string): boolean {
		let current = moment();
		let dayStartTime = moment('08:00:00', 'HH:mm:ss');
		let dayEndTime = moment('20:10:00', 'HH:mm:ss');
		let nightStartTime = moment('20:00:00', 'HH:mm:ss');
		let midNightTime = moment('23:59:59', 'HH:mm:ss');
		let currDate: string;
		let currShift: string;
		
		if(current.isBetween(dayStartTime, dayEndTime)){
			currShift = "Day";
			currDate = current.format('YYYYMMDD');
		}else{
			currShift = "Night";
			
			if(current.isBetween(nightStartTime, midNightTime))				
				currDate = current.format('YYYYMMDD');
			else
				currDate = current.add(-1, 'days').format('YYYYMMDD');
		}
		
		return (paramDate === currDate && paramShift === currShift);
	}

	ngOnDestroy(){
		this.sub.unsubscribe();
		this._machineComm.change.unsubscribe();
	}

	ShowContent(_machine: MachineListIPModel, _identify: string) {
		const graph_width = 500;
		const graph_height = 140;
		const margin_graphLeft = 50;
		const margin_graphRight = 100;
		const margin_graphTop = 30;
		const bar_width = 60;
		const bar_gap = 10;

		let data: MachineWorkingTimeModel;
		let firstOffset: number;
		let machine: string
		
		let datasource: MachinePerfModel = {mcd: '', mc_desc: '', working: []};
		datasource.mcd = _machine.i_ind_content.trim();
		datasource.working = [];

		machine = _machine.i_ind_content.trim() + "-" + _identify;

		let original_name = 'prod-svg-' + machine;
		let name = '#' + original_name;

		let svg = d3.select('#prod-container-' + machine)
			.append('svg')
			.attr('id', original_name)
			.attr('width',  graph_width + 50)
			.attr('height', graph_height)
			.attr('style', 'outline: thin solid #f5f5dc');

		/* Object color linearGradient	*/
		let arrMain: string[] = ['mainGradientB', 'mainGradientW', 'mainGradientM', 'mainGradientO'];
		let colorTop: string[] = ['#ffaf4b', '#58fc46', '#f25a58', '#87e0fd'];
		let colorBot: string[] = ['#ff920a', '#34e021', '#d84240', '#05abe0'];

		arrMain.map((a, i) => {
			var svgDefs = svg.append('defs');
			var gradient = svgDefs.append('linearGradient')
					.attr('id', a)
					.attr("x1", "0%")
					.attr("y1", "0%")
					.attr("x2", "0%")
					.attr("y2", "100%");
			gradient.append('stop')
					.attr('stop-color', colorTop[i])
					.attr('offset', '0%');
			gradient.append('stop')
					.attr('stop-color', colorBot[i])
					.attr('offset', '100%');
		});
		/* --Object color linearGradient	*/
		
		var color = d3.scaleLinear()
			.domain([0, 50, 100])
			.range(["red", "yellow", "green"]);

		svg = d3.select(name);
	
		this.sub = this.timer.subscribe(t => {
			this.machineDS.getJobResultDashboardIP(this.machineMessage.productionDate, _machine.i_ind_content.trim(), _machine.ip_address.trim())
				.subscribe(data => {
					this.sub = null;
					svg.selectAll('rect').remove();
					svg.selectAll('text').remove();
					
					/*
						data_max always update to real time;
					*/
					this.data_max = this.checkCurrentDateShift(this.currentDate, this.currentShift) ? moment().toDate() 
						: moment(moment(this.data_min).add(12, 'hours').toDate()).add(10, 'minutes').toDate();					

					const data_filtered = data.filter( element => {
						return (element.type == 'W' || element.type == 'O')
							&& moment(element.prc_start).valueOf() < moment(element.prc_end).valueOf()
							&& moment(element.prc_end).valueOf() > moment(this.data_min).valueOf() 
							&& moment(element.prc_start).valueOf() < moment(this.data_max).valueOf()
					});
					
					datasource.working = [];
				
					data_filtered.forEach(element => {
						let workingTime: MachineWorkingTimeModel = {type: '', jobno: '',
						item: '', start: new Date(), finish: new Date()};

						workingTime.type = element.type;
						workingTime.jobno = element.jobno;
						workingTime.item = element.i_sim_item_cd;
						workingTime.start = (moment(element.prc_start).valueOf() <= moment(this.data_min).valueOf()) ? this.data_min : moment(element.prc_start).toDate();
						workingTime.finish = (moment(element.prc_end).valueOf() >= moment(this.data_max).valueOf()) ? this.data_max : moment(element.prc_end).toDate();
						datasource.working.push(workingTime);
						
					});
					
					/* scaleLinear as percentage	*/
					let xscale = d3.scaleLinear()
						.domain([0, 100])
						.range([margin_graphLeft, graph_width - margin_graphRight]);

					xscale.clamp(true);
					
					let diff_now = moment.duration(moment(this.data_max).diff(this.data_min)).asMinutes();

					/*	Background (Static)	
					svg = d3.select(name)
						.append('rect')
						.attr( 'x', xscale(0))
						.attr( 'y', margin_graphTop)
						.attr( 'rx', 20)
						.attr( 'ry', 20)
						.attr( 'height', bar_width )
						.attr( 'width', xscale(100))
						.attr( 'fill', '#e6e6e6');
						
						*/
					/*	./Background (Static)	*/
					
					svg = d3.select(name);
					
					let sum_diff_time: number = 0;
					datasource.working.map((e, i) => {
						var duration = moment.duration(moment(e.finish).diff(e.start));
						sum_diff_time += duration.asMinutes();
					});

					let percent: number = this.calPercentofTime(sum_diff_time, diff_now);

					svg.append('rect')
						.attr( 'class', 'datasource')
						.attr( 'x', xscale(0))
						.attr( 'y', margin_graphTop - 10)
						.attr( 'rx', 20)
						.attr( 'ry', 20)
						.attr( 'height', bar_width )
						.attr( 'width', xscale(percent) - xscale(0))
						.attr( 'fill', color(percent))
						.attr( 'stroke', color(percent))
						;
					
					
					if(percent > 100){
						percent = 100;
					}
					
					svg.append('text')
						.attr('x', (xscale(percent) - xscale(0)) + 80 )
						.attr('y', 50)
						.attr('font-family', 'Tahoma')
						.attr('font-size', '1.5em')
						.attr('color', 'black')
						.text(percent.toFixed(2).toString() + '%');
						
					if(sum_diff_time > diff_now){
						sum_diff_time = diff_now;
					}
					svg.append('text')
						.attr('x', xscale(0))
						.attr('y', 100)
						.attr('font-family', 'Tahoma')
						.attr('font-size', '1em')
						.attr('color', 'black')
						.text('Total availablity (Min) :: ' + sum_diff_time.toFixed(2) + '/' + diff_now.toFixed(2));
				});
		});
	}
	
	private calPercentofTime(time: number, all_time: number): number{
		return (time/all_time) * 100;
	}

}