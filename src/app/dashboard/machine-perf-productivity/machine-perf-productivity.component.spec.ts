import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachinePerfProductivityComponent } from './machine-perf-productivity.component';

describe('MachinePerfProductivityComponent', () => {
  let component: MachinePerfProductivityComponent;
  let fixture: ComponentFixture<MachinePerfProductivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachinePerfProductivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachinePerfProductivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
