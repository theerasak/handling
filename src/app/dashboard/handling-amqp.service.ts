import { Injectable } from '@angular/core';
import { Connection, Message, Queue, Binding, Exchange } from 'amqp-ts';

@Injectable()
export class HandlingAmqpService {

    private amqpServer = 'amqp://guest:guest@192.168.99.100:5672';
    private connection;
    private exchange;
    private queue;
    private dataMessage: string[];

    constructor() {
      this.connection = new Connection(this.amqpServer);
      this.exchange = this.connection.declareExchange('nmth.msg');
      this.queue = this.connection.declareQueue('amqp.a01.1.q');
    }

    consume(): string[] {

      this.queue.bind(this.exchange);
      this.queue.activateConsumer((message) => {
        console.log('Message received: ' + message.getContent());
        this.dataMessage.push(message.getContent());
      });
      return this.dataMessage;
    }
}
