import { TestBed, inject } from '@angular/core/testing';

import { HandlingAmqpService } from './handling-amqp.service';

describe('HandlingAmqpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HandlingAmqpService]
    });
  });

  it('should be created', inject([HandlingAmqpService], (service: HandlingAmqpService) => {
    expect(service).toBeTruthy();
  }));
});
