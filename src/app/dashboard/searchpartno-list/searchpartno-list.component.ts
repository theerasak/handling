import { Component, OnInit } from '@angular/core';
import { HandlingCommService,  HandlingCommunicationInterface } from '../handling-comm.service';

@Component({
  selector: 'app-searchpartno-list',
  templateUrl: './searchpartno-list.component.html',
  styleUrls: ['./searchpartno-list.component.css']
})
export class SearchpartnoListComponent implements OnInit {
  partno: string;
  refreshToggle: boolean;
  errorMessage: string;
  handlingMessage: HandlingCommunicationInterface;
  constructor(private _handlingComm: HandlingCommService) {
  }

  ngOnInit() {
    this.refreshToggle = true;
  }

  updateRequest() {
    console.log(this.partno);
    this._handlingComm.submitPartno(this.partno);
  }


}
