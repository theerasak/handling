import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchpartnoListComponent } from './searchpartno-list.component';

describe('SearchpartnoListComponent', () => {
  let component: SearchpartnoListComponent;
  let fixture: ComponentFixture<SearchpartnoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchpartnoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchpartnoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
