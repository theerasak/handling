import { TestBed, inject } from '@angular/core/testing';

import { HandlingCommService } from './handling-comm.service';

describe('HandlingCommService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HandlingCommService]
    });
  });

  it('should be created', inject([HandlingCommService], (service: HandlingCommService) => {
    expect(service).toBeTruthy();
  }));
});
