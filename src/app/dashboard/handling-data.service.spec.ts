import { TestBed, inject } from '@angular/core/testing';

import { HandlingDataService } from './handling-data.service';

describe('HandlingDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HandlingDataService]
    });
  });

  it('should be created', inject([HandlingDataService], (service: HandlingDataService) => {
    expect(service).toBeTruthy();
  }));
});
