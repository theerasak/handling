import { Component, OnInit, AfterViewInit  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MachineListService, MachineListModel, MachineListIPModel } from '../machine-list.service';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MachineCommService, MachineCoummunicationInterface } from '../machine-comm.service';
import 'rxjs/add/operator/map';
import * as moment from 'moment';

@Component({
  selector: 'app-machine-perf-list',
  templateUrl: './machine-perf-list.component.html',
  styleUrls: ['./machine-perf-list.component.css']
})
export class MachinePerfListComponent implements OnInit, AfterViewInit  {

  _listMachine: MachineListIPModel[] = [];
  _listMachine2: MachineListIPModel[] = [];
  hasParam: boolean = false;
  
  calendarDate: Date;
  productionDate: string;
  selectedShift: string;

  constructor(private route: ActivatedRoute, private mcListService: MachineListService
			, private machineCommService: MachineCommService) {
	
	this.productionDate = moment().format('YYYYMMDD');
	this.selectedShift = "Day";
	
	this.hasParam = false;
  }

  ngOnInit() {
	let list_param: string = null;
	this.route.queryParams
		.subscribe(p => {
			this.mcListService.getMachineListIP(this.setFormatMacList(p.mcs))
				.subscribe(obj => {
					obj.forEach(o => {
						if(o.is_production_mode){
							this._listMachine.push(o);
						}
					});
					setTimeout(() => {
						this.updateRequest();
					}, 1000);
				},error => { console.log("ERROR :: " + error); },() => {
					this.updateRequest();
				});
		});
  }
  
  ngAfterViewInit(){
  }
  
  private _to2digit(n: number) {
    return ('00' + n).slice(-2);
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
	this.productionDate = event.value.getFullYear() + this._to2digit(event.value.getMonth() + 1 ) + this._to2digit(event.value.getDate());
  }
  
  updateRequest(){
	this.machineCommService.submitData(this.productionDate, this.selectedShift);
  }

  private removeSyntax(source: string, targeted: string){
	return source.trim().split('.').join('');
  }
  
  private setFormatMacList(macs: string): string {
	if(macs == null){
		return null;
	}else{
		let array_macs: string[] = macs.trim().split(',');
		let list_single_quote: string[] = [];
		array_macs.forEach(m => {
			list_single_quote.push("'"+m+"'");
		});
		
		return list_single_quote.join();
	}
  }
}
