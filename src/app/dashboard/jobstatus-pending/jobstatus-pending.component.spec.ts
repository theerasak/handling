import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobstatusPendingComponent } from './jobstatus-pending.component';

describe('JobstatusPendingComponent', () => {
  let component: JobstatusPendingComponent;
  let fixture: ComponentFixture<JobstatusPendingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobstatusPendingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobstatusPendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
