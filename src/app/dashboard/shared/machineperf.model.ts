export class MachinePerfModel {
    mcd: string;
    mc_desc: string;
    working: MachineWorkingTimeModel[];
}

export class MachineWorkingTimeModel {
    type: string;
    jobno: string;
    item: string;
    start: Date;
    finish: Date;
}
