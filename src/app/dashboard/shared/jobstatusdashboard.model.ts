export class JobStatusDashboardModel {
         u_plan_date: string;
         u_ind_content: string;
         u_shift: string;
         u_revision: number;
         u_timestamp: number;
         u_client: string;
         i_seiban: string;
         i_plan_ind_content: string;
         i_line: string;
         i_shift: string;
         i_pl_ind_content: string;
         i_sim_ind_content: string;
         i_po_detail_no: string;
         i_plan_item_cd: string;
         i_sim_item_cd: string;
         i_sim_po_qty: number;
         seqno: number;
         seiban_remark: string;
         i_fac_cd: string;
         jobno: string;
         prc_start;
         prc_end;
         state: string;
         i_plan_remark1: string;
         i_sch_lot_no: string;
         i_stock_location: string;
		 type: string;
}
