import { Component } from '@angular/core';
import { DashboardMainComponent } from './dashboard/dashboard-main/dashboard-main.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
}
